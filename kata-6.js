module.exports.filterMultiples = function (arr, num) {
    return arr.filter(n => n%num !== 0);
}




module.exports.matcher = function (sourceA, sourceB) {
    const newArray = [];
    const longest = Math.max(sourceA.length, sourceB.length);

    for (let i = 0; i < longest; i++) {
        if (sourceB.includes(sourceA[i])) {
            newArray.push(sourceA[i])
        }
    }
    return newArray
};


module.exports.emailToName = function(emails) {
    return emails.map(email => {
        const nameArr = email.split('@');
        return nameArr[0].replace(/\./g, ' ').replace(/\-/g, ' ');
    })
}


