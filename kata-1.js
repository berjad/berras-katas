// 1. Create a function that can accept undefined number of parameters
module.exports.paramFunction = function(...args) {
    return args.length;
}
// 2. Create an array with 5 string elements (add  your name as on of the elements)
const array = ['one', 'cheese', 'Berra', 'pizza', 'beer'];
module.exports.myArray = array;

// 3. Write a loop that prints each of the 5 elements in the above array
// for(let item = 0; item < array.length; item++) {
//     console.log(array[item]);
// }

// for(const item of array) {
//     return (item);
// }

array.forEach(item => item);
    
    

// 4. Use the filter to create a new array that EXCLUDES your name
  
module.exports.filterName = function(array) {
    return array.filter(item => item != 'Berra');
}

// 5. Create a new array using the .map() function. 
// The new array should have 5 objects with 2 properties (id, value) -> 
// use the value of the array from exercise 2.


module.exports.createNewArrayMap = function(array) {
    return array.map((val, index) => {
        return {
            id: index,
            value: val
        };
    });
}

