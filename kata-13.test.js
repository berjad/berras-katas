const { getBudgets, vreplace, isIsogram } = require('./kata-13');

describe('kata-13', () => {

    test('sum of budgets', () => {
        const people = [
            {name: 'John', age: 21, budget: 29000},
            {name: 'Steve', age: 32, budget: 32000},
            {name: 'Martin', age: 16, budget: 1600}
        ]
        expect(getBudgets(people)).toBe(62600)
    })

    test('replace vowels in string with an other string', () => {
        
        expect(vreplace('stuffed jalapeno poppers','e')).toBe('steffed jelepene peppers');
    });

    test('is a string isogram, true or false', () => {
        expect(isIsogram('Algorism')).toBe(true);
    })
    test('is a string isogram, true or false', () => {
        expect(isIsogram('PaSwordS')).toBe(false);
    })
    test('is a string isogram, true or false', () => {
        expect(isIsogram('Consecutive')).toBe(false);
    })
})