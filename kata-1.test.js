const { paramFunction, myArray, filterName, createNewArrayMap} = require('./kata-1');

describe('Kata', () => {
    test('rest parameter', () => {
        expect(paramFunction(1,2,3,4)).toBe(4);
    });

    test('array with my name should be 5 elements', () => {
        expect( myArray.length ).toBe( 5 );
    })

    test('same array should not include Berra', () => {
        const source = ['one', 'cheese', 'Berra', 'pizza', 'beer'];
        const exp = ['one', 'cheese', 'pizza', 'beer'];
        expect (filterName(source)).toEqual(exp);
    });

    test('should give 5 objects from source array', () => {

        const source = ['one', 'cheese', 'Berra', 'pizza', 'beer'];
        const exp = [{
            id: 0,
            value: 'one'
        },
        {
            id: 1,
            value: 'cheese'
        },
        {
            id: 2,
            value: 'Berra'
        },
        {
            id: 3,
            value: 'pizza'
        },
        {
            id: 4,
            value: 'beer'
        }

        ];

        expect(createNewArrayMap(source)).toEqual(exp);

    });

});