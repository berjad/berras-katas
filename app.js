// Higher Order Functions


const companies = [
    {name: "Company One", category: "Finance", start: 1982, end:2005},
    {name: "Company Two", category: "Retail", start: 1992, end:2008},
    {name: "Company Three", category: "Auto", start: 1999, end:2007},
    {name: "Company Four", category: "Retail", start: 1989, end:2010},
    {name: "Company Five", category: "Technology", start: 2009, end:2014},
    {name: "Company Six", category: "Finance", start: 1987, end:2010},
    {name: "Company Seven", category: "Auto", start: 1986, end:1996},
    {name: "Company Eight", category: "Technology", start: 2011, end:2016},
    {name: "Company Nine", category: "Retail", start: 1981, end:1989}
];

const ages = [ 33, 12, 20, 16, 5, 54, 21, 44, 61, 13, 15, 45, 25, 64, 32 ];

// // forEach ---------------------------------------------------------------

// companies.forEach((company) => {
//     console.log(company);
//     console.log(company.name);
// });

// // filter on age 21 and older ------------------------------------------------------

// const allowedToDrink = ages.filter((age) => {
//     if(age >= 21) {
//         return true;
//     }
// });
// console.log(allowedToDrink);


// // or

// const canDrink = ages.filter(age => age >= 21);
// console.log(canDrink);

// // filter retail companies

// const retailCompanies = companies.filter(company => company.category === 'Retail');
// console.log(retailCompanies);

// // filter companies started during 1980 - 1989
// const companyStart = companies.filter(company => (company.start >= 1980 && company.start < 1990));
// console.log(companyStart);

// // filter companies that lasted more then 10 year
// const lastedCompanies = companies.filter(company => (company.end - company.start >= 10 ));
// console.log(lastedCompanies);

// // map creat array of company names -------------------------------------------------------

// const compName = companies.map(company => `${company.name} Started Year ${company.start}`);
// console.log(compName);

// // map square of ages

// const squareAges = ages.map(age => Math.sqrt(age));
// console.log(squareAges);

// // map age * 2
// const agesTimesTwo = ages.map(age => age * 2)
// console.log(agesTimesTwo);

// // map square of age * 2
// const ageMap = ages
// .map(age => Math.sqrt(age))
// .map(age => age * 2);
// console.log(ageMap);

// // sort on companies on start year
// const sortedCompanies = companies.sort((compA, compB) => (compA.start > compB.start ? 1 : -1));
// console.log(sortedCompanies);

// // sort ages
// const sortAges = ages.sort((a,b) => a - b );
// console.log(sortAges);

// // reduce total years of ages

// const ageSum = ages.reduce((total, age) => total + age, 0);
// console.log(ageSum);

// // get total years for all companies

// const totalYears = companies.reduce((total, company) => total + (company.end - company.start), 0);
// console.log(totalYears);




// // forEach ---------------------------------------------------------------
// companies.forEach((i) => {
//     console.log(i);
//     console.log(i.name);
//     console.log(i.category);
//     console.log(i.start, i.end);
// })

// filter on age 21 and older ------------------------------------------------------

const validAge = ages.filter((age) => {
    if (age >= 21) {
        return true;
    }
        
});
console.log(validAge);

// or
const allowedToDrink = ages.filter((age) => age >= 21);
console.log(allowedToDrink);


// filter retail companies
const retailCompanies = companies.filter((company) => company.category === 'Retail');
console.log(retailCompanies);


// filter companies started during 1980 - 1989
const companiesStarted = companies.filter((company) => company.start >= 1980 && company.start < 1990 );
console.log(companiesStarted);

// filter companies that lasted more then 10 year
const lastedCompanies = companies.filter((company) => company.end - company.start >= 10);
console.log(lastedCompanies);

// map creat array of company names -------------------------------------------------------
const compName = companies.map((company) => company.name);
console.log(compName);

// map square of ages
const squareAges = ages.map((age) => Math.sqrt(age));
console.log(squareAges);

// map age * 2
const doubleAge = ages.map((age) => age * 2);
console.log(doubleAge);

// map square of age * 2
const moreAge = ages
.map((age) => Math.sqrt(age))
.map((age) => age * 2);
console.log(moreAge);

// sort on companies on start year
const sortedCompanies = companies.sort((a, b) => a.start - b.start);
console.log(sortedCompanies);

// sort ages
const sortAges = ages.sort((a, b) => a - b);
console.log(sortAges);

// reduce total years of ages
const totalYears = ages.reduce((total, age) => total + age, 0);
console.log(totalYears);


// get total years for all companies
const totalComp = companies.reduce((total, company) => total + (company.end - company.start), 0);
console.log(totalComp);
