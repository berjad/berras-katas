
function describeNum(num) {
    const startSent = 'The most ';
    const endSent = `number is ${num}!`;
    const phrase = [
        [1, 'brilliant '],
        [2, 'exciting '],
        [3, 'fantastic '],
        [4, 'virtuous '],
        [5, 'heart-warming '],
        [6, 'tear-jerking '],
        [7, 'beautiful '],
        [8, 'exhilarating '],
        [9, 'emotional '],
        [10, 'inspiring '],
        [11, 'mind-blowing']
    ];
    const middle = phrase.reduce(function (prev, curr) {
        return prev += num % parseInt(curr[0]) === 0 ? curr[1] : '';
    }, '');

    return startSent + middle + endSent;
}

console.log(describeNum(234));






function billSplit(spicy, cost) {
    let me = 0, friend = 0;
    for (let i = 0; i < spicy.length; i++) {
      if (spicy[i] === "S") {
        me += cost[i];
      }
      else { 
        me += cost[i] / 2;
        friend += cost[i] / 2;
      }
    }
    return [me, friend];
  }
 console.log(billSplit(['S', 'N'], [41, 10]));
 