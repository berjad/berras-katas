const { sevenBoom, Book } = require('./kata-15');

describe('kata-15', () => {

    test('sevenBoom([1, 2, 3, 4, 5, 6, 7]) ➞ "Boom!"', () => {
      
        expect(sevenBoom([1, 2, 3, 4, 5, 6, 7])).toBe("Boom!");
    })

    test('sevenBoom([8, 6, 33, 100]) ➞ "there is no 7 in the array"', () => {
      
        expect(sevenBoom([8, 6, 33, 100])).toBe("there is no 7 in the array");
    })
    test('sevenBoom([2, 55, 60, 97, 86]) ➞ "Boom!"', () => {

        expect(sevenBoom([2, 55, 60, 97, 86])).toBe("Boom!");
    })
})
describe('2. Book Shelf', ()=>{

    test('book.getTitle() -> Title: Harry Potter', ()=>{
        const book = new Book('Harry Potter - J.K. Rowling');
        expect( book.getTitle() ).toBe( 'Title: Harry Potter' );
    });

    test('book.getAuthor() -> Author: J.K. Rowling', ()=>{
        const book = new Book('Harry Potter - J.K. Rowling');
        expect( book.getAuthor() ).toBe( 'Author: J.K. Rowling' );
    });

    test('book.getFullDescription() -> Harry Potter - J.K. Rowling', ()=>{
        const book = new Book('Harry Potter - J.K. Rowling');
        expect( book.getFullDescription() ).toBe( 'Harry Potter - J.K. Rowling' );
    });


    test('book.getTitle() -> Title: Hitchickers Guide to the Galaxy', ()=>{
        const book = new Book('Hitchickers Guide to the Galaxy - D. Adams');
        expect( book.getTitle() ).toBe( 'Title: Hitchickers Guide to the Galaxy' );
    });

    test('book.getAuthor() -> Author: D.Adams', ()=>{
        const book = new Book('Hitchickers Guide to the Galaxy - D. Adams');
        expect( book.getAuthor() ).toBe( 'Author: D. Adams' );
    });

    test('book.getFullDescription() -> Hitchickers Guide to the Galaxy - D. Adams', ()=>{
        const book = new Book('Hitchickers Guide to the Galaxy - D. Adams');
        expect( book.getFullDescription() ).toBe( 'Hitchickers Guide to the Galaxy - D. Adams' );
    });

    

});
