function sevenBoom(numbers) {
    let luck = "Boom!";
    let noLuck = "there is no 7 in the array";
        if(numbers.join('').includes('7')) {
            return luck;
        } else {
            return noLuck;
        }
    }

module.exports.sevenBoom = sevenBoom;


function Book(book) {
    const fullBook = book;
    this.title = fullBook.split('-')[0].trim();
    this.author = fullBook.split('-')[1].trim();

    this.getTitle = function() {
        return "Title: " + this.title;
    }
    this.getAuthor = function() {
        return "Author: " + this.author;
    }
    this.getFullDescription = function() {
        return fullBook;
    }

}

module.exports.Book = Book;


