const myArray = [2,2,2];

const equals = myArray.every(function(value, index, array) {
    index = 0;
    return value === array[index];
});
console.log(equals);

// let myMatch = array.forEach((item) => {
//     return item;
// });


// console.log(myMatch());

const anagram = (str1, str2) => {
    const compare = (first, next) => {
     return first.split('').sort().join('') === next.split('').sort().join('');
    }
    return str1.length !== str2.length ? false : compare(str1, str2);
  }
console.log(anagram('cheese', 'bacon'));
