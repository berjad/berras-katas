function chunk(array, num) {
    const result = [];
    while(array.length) {
        result.push(array.splice(0, num));
    }
    return result;
}

module.exports.chunk = chunk;