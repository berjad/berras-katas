const { chunk } = require('./kata-9');


describe('kata-9', () => {
    test('Split argument 1 into several smaller array. the number of array should equal second argument', () => {
        expect(chunk(['a', 'b', 'c', 'd'], 2)).toEqual([['a', 'b'], ['c', 'd']]);
    })
})