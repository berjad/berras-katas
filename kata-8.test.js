const { mostWanted } = require('./kata-8');

describe('Kata-8', () => {

    test('the most wanted letter', () => {
        const str = "Hello World!"
        expect(mostWanted(str)).toBe("l");
    })
    test('the most wanted letter', () => {
        const str = "How do you do?"
        expect(mostWanted(str)).toBe("o");
    })
    test('the most wanted letter', () => {
        const str = "One"
        expect(mostWanted(str)).toBe("o");
    })
    test('the most wanted letter', () => {
        const str = "Oops"
        expect(mostWanted(str)).toBe("o");
    })
})