const { square, charFreq, minMaxWords, filterLongWords } = require('./kata-5');

test('square array of numbers', () => {
    const numbers = [2, 4, 8];
    expect( square(numbers) ).toEqual([4, 16, 64]);
});

test('count the number of characters in string', ()=>{
    const str = 'aabbcd';
    expect( charFreq( str ) ).toEqual( { a: 2, b: 2, c:1, d: 1 } );
});

test('get the longest and shortest word from an array', ()=>{

    const expected = ['c', 'javascript']
    const words = ['one', 'c', 'javascript','tacocat'];

    expect( minMaxWords(words) ).toEqual( expect.arrayContaining(expected) );

});

test('remove words longer than 4 characters', ()=>{

    const words = ['wow', 'javascript', 'no', 'mysterious', 'java', 'taco', 'cat']
    expect( filterLongWords(4, words) ).toEqual( ['wow', 'no', 'java', 'taco', 'cat'] );

});

// test('check if given word is palindrome', ()=>{

//     const word = 'tacocat';
//     expect( isPalindrome( word ) ).toBe(true);

// })