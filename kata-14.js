// 1.
function bbqSkewers(skewers) {
    const result = [0, 0];

    skewers.forEach(function(skewer) {
        if(skewer.indexOf('x') > -1) {
            result[1] += 1;
        } else {
            result[0] += 1;
        }
    }) 
        return result;
}

module.exports.bbqSkewers = bbqSkewers;




// 2.
function removeDups(array) {
    
    return [...new Set(array)]

}

module.exports.removeDups = removeDups;


// 3.
function XO(src) {
    const xs = (src.match(/x/gi) || []).length;
    const xo = (src.match(/o/gi) || []).length;
    return xs === 0 && xo === 0 || xs === xo;
}

module.exports.XO = XO;
