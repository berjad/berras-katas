const { bbqSkewers, removeDups, XO } = require('./kata-14');

describe('kata-14', () => {
    test(`Given "--oooo-ooo--",
    "--xx--x--xx--",
    "--o---o--oo--",
    "--xx--x--ox--",
    "--xx--x--ox--" -> [2, 3]`, () => {
        const source = [
            "--oooo-ooo--",
            "--xx--x--xx--",
            "--o---o--oo--",
            "--xx--x--ox--",
            "--xx--x--ox--"
        ];
        expect(bbqSkewers(source)).toEqual([2, 3]);
    });
    test(`Given "--oooo-ooo--",
            "--xxxxxxxx--",
            "--o---",
            "-o-----o---x--",
            "--o---o-----" -> [3, 2]`, () => {

        const source = [
            "--oooo-ooo--",
            "--xxxxxxxx--",
            "--o---",
            "-o-----o---x--",
            "--o---o-----"
        ];

        expect(bbqSkewers(source)).toEqual([3, 2]);
    });

});
describe('2. Remove Duplicates from an Array', () => {

    test('removeDups([1, 0, 1, 0]) ➞ [1, 0]', () => {
        const source = [1, 0, 1, 0];
        expect(removeDups(source)).toEqual([1, 0]);
    });

    test('removeDups(["The", "big", "cat"]) ➞ ["The", "big", "cat"]', () => {
        const source = ["The", "big", "cat"];
        expect(removeDups(source)).toEqual(["The", "big", "cat"]);
    });

    test('removeDups(["John", "Taylor", "John"]) ➞ ["John", "Taylor"]', () => {
        const source = ["John", "Taylor", "John"];
        expect(removeDups(source)).toEqual(["John", "Taylor"]);
    });


});


describe('3. Xs and Os, Nobody Knows', () => {

    test('XO("ooxx") ➞ true', () => {
        expect( XO("ooxx") ).toBe(true);
    });

    test('XO("xooxx") ➞ false', () => {
        expect( XO("xooxx") ).toBe(false);
    });

    test('XO("ooxXm") ➞ true', () => {
        expect( XO("ooxXm") ).toBe(true);
    });

    test('XO("zpzpzpp") ➞ true', () => {
        expect( XO("zpzpzpp") ).toBe(true);
    });

    test('XO("zzoo") ➞ false', () => {
        expect( XO("zzoo")  ).toBe(false);
    });

});