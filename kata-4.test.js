const { encode, decode, encodeAtChar } = require('./kata-4');


describe('encode', () => {
    test('should change "javascript" to 10-1-22-1-19-3-18-9-16-20', () => {
        expect(encode('javascript')).toBe('10-1-22-1-19-3-18-9-16-20');
    });

    test('should change "hello javascript" to 8-5-12-12-15-/s-10-1-22-1-19-3-18-9-16-20', () => {
        expect(encode('hello javascript')).toBe('8-5-12-12-15-/s-10-1-22-1-19-3-18-9-16-20');
    });

    test('should change "" to false', () => {
        expect(encode('')).toBe(false);
    });
})


describe('decode', () => {
    test('should change 10-1-22-1-19-3-18-9-16-20 to "javascript"', () => {
        expect(decode('10-1-22-1-19-3-18-9-16-20')).toBe('javascript');
    });

    test('should change 8-5-12-12-15-/s-10-1-22-1-19-3-18-9-16-20 to "hello javascript"', () => {
        expect(decode('8-5-12-12-15-/s-10-1-22-1-19-3-18-9-16-20')).toBe('hello javascript');
    });

    test('should change "" to false', () => {
        expect(decode('')).toBe(false);
    });
})

describe('encodeAtChar', () => {
    test('should change "javascript" to 10-1-22-1-19-3-18-9-16-20', () => {
        expect(encodeAtChar('javascript')).toBe('10-1-22-1-19-3-18-9-16-20');
    });

    test('should change "hello javascript" to 8-5-12-12-15-/s-10-1-22-1-19-3-18-9-16-20', () => {
        expect(encodeAtChar('hello javascript')).toBe('8-5-12-12-15-/s-10-1-22-1-19-3-18-9-16-20');
    });

    test('should change "" to false', () => {
        expect(encodeAtChar('')).toBe(false);
    });
})
