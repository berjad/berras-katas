/**--------------------------------------------------------------------------------------------------------
 * 1. Write a function that returns the length of any given string
 *--------------------------------------------------------------------------------------------------------*/

function getLength(string) {
    return string.length;
}

/**--------------------------------------------------------------------------------------------------------
 * 2. Given a string “JavaScript is Awesome”, write a loop/function 
 * that prints every second character ( a a c i t )
 *--------------------------------------------------------------------------------------------------------*/
const myString = 'JavaScript is Awesome';
let newString = '';
for(let i = 1; i < myString.length; i+= 2) {
    newString += myString[i];
}
console.log(newString);

/**-------------------------------------------------------------------------------------------------------- 
 * 3. Make use a native JavaScript function to extract the word JavaScript from the following string: 
 * “I want to code in JavaScript every single day!”
 *--------------------------------------------------------------------------------------------------------*/
const strToUse = "I want to code in JavaScript every single day!";
const word = "JavaScript";
const start = strToUse.indexOf(word);
const end = start + word.length;
const extractWord = strToUse.slice(start, end);
console.log(extractWord);


/**-------------------------------------------------------------------------------------------------------- 
 * 4. Write a function that accepts 1 argument that is a string. 
 * The function should correct the casing in a sentence: “fIX ThE WoRDS witH jaVASCRIPT” 
 * and return the new string.
 --------------------------------------------------------------------------------------------------------*/


/**--------------------------------------------------------------------------------------------------------
 * 5. Replace all occurrences of the word: “PHP” in the string: “I love PHP, I want to learn PHP”
 * with the word JavaScript
 --------------------------------------------------------------------------------------------------------*/


// REGEX - Look it up...

// g - global
// i - case insensitive, leave the i to replace both upper and lower case occurences.

