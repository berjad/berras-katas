const { largerThan, convertToRoman, totalAnimals, wordLengths } = require('./kata-7');

describe('Kata-7', () => {
    // test('test sum of array and max number true', () => {
    //     const array = [1,2,3,4];
    //     const sumOfArray = array.reduce((a,b) => a + b, 0);
    //     const maxNumber = 8;
    //     expect(largerThan(sumOfArray > maxNumber)).toBe(true);
    // })
    test('test sum of array and max number false', () => {
        const array = [1,2,3,4];
        const sumOfArray = array.reduce((a,b) => a + b, 0);
        const maxNumber = 15;
        expect(largerThan(sumOfArray > maxNumber)).toBe(false);
    })

    test('how many legs on chickens, cows and pigs', () => {
        expect(totalAnimals(2,3,5)).toBe(36);
        expect(totalAnimals(1,2,3)).toBe(22);
        expect(totalAnimals(5,2,8)).toBe(50);
    })

    test('word length', () => {
        expect(wordLengths(['hello', 'world'])).toStrictEqual([5, 5]);
        expect(wordLengths(['Helloween', 'Thanksgiving', 'Christmas'])).toStrictEqual([9, 12, 9]);
        expect(wordLengths(['She', 'sells', 'seashells', 'down', 'by', 'the', 'seashore'])).toStrictEqual([3, 5, 9, 4, 2, 3, 8]);
        
    })

    test('convert modern number to roman', () => {
        expect(convertToRoman(6)).toBe('VI'),
        expect(convertToRoman(76)).toBe('LXXVI'),
        expect(convertToRoman(13)).toBe('XIII'),
        expect(convertToRoman(44)).toBe('XLIV'),
        expect(convertToRoman(3999)).toBe('MMMCMXCIX')
    })
})