const { filterMultiples, matcher, emailToName } = require('./kata-6');

describe('kata-6', () => {
    test('return multiples of 2', () => {
        const source = [1,2,3,4];
        const exp = [1,3];

        expect( filterMultiples( source, 2 ) ).toEqual( exp );
    });

    test('matcher() should return an array with [taco and cheese]', ()=>{
        const sourceA = [1, 'pizza', 94, 'taco', 'cheese'];
        const sourceB = [2, 'cheese', 'taco', 9];
        const expected = ['taco', 'cheese'];
        expect( matcher(sourceA, sourceB) ).toEqual( expect.arrayContaining(expected) );
    });

    test('get 4 names without email', ()=>{
        const source = ['dewald.els@gmail.com', 'luke-skywalker@resistance.space', 'daddy-vader@deathstar.com', 'han.shotfirst@gmail.com'];
        const expected = ['dewald els', 'luke skywalker', 'daddy vader', 'han shotfirst'];
        expect( emailToName( source ) ).toEqual( expected );
     });

     test('get 4 names from email and remove all . and - occurences', ()=>{
        const source = ['de.wald.els@gmail.com', 'luke-skywa.lker@resistance.space', 'd-addy-vader@deathstar.com', 'han.shot.first@gmail.com'];
        const expected = ['de wald els', 'luke skywa lker', 'd addy vader', 'han shot first'];
        expect( emailToName( source ) ).toEqual( expected );
     });

})