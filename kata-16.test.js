const { sweetestIcecream } = require('./kata-16');

describe('kata-16', () => {

    test('sweetestIcecream([ice1, ice2, ice3, ice4, ice5]) ➞ 23', () => {

        expect(sweetestIcecream([ice1, ice2, ice3, ice4, ice5])).toBe(23);
    })
    test('sweetestIcecream([ice3, ice1]) ➞ 23', () => {

        expect(sweetestIcecream([ice3, ice1])).toBe(23);
    })
    test('sweetestIcecream([ice3, ice5]) ➞ 23', () => {

        expect(sweetestIcecream([ice3, ice5])).toBe(17);
    })

})