const ALHPA_KEYS = {
    a: 1,
    b: 2,
    c: 3,
    d: 4,
    e: 5,
    f: 6,
    g: 7,
    h: 8,
    i: 9,
    j: 10,
    k: 11,
    l: 12,
    m: 13,
    n: 14,
    o: 15,
    p: 16,
    q: 17,
    r: 18,
    s: 19,
    t: 20,
    u: 21,
    v: 22,
    w: 23,
    x: 24,
    y: 25,
    z: 26,
    space: '/s'
};

function encode(str) {
    return str.trim() ? str.split('').map(c => {
        if(c == ' ') return ALHPA_KEYS.space;
        else return ALHPA_KEYS[c.toLowerCase()];
    }).join('-') : false;
}

function decode(str) {
    return str.trim() ? str.split('-').map(c => {
        if(c == ALHPA_KEYS.space) return ' ';
        else {
            return Object.keys(ALHPA_KEYS).find(k => ALHPA_KEYS[k] == c);
        }
    }).join('') : false;
}

const CALIBRATE = 96;
const SPACE_CHAR = '/s';

function encodeAtChar(str){
    return str.trim() ? str.split('').map((c, i) => {
        if (c == ' ') return SPACE_CHAR;
        else return str.charCodeAt(i) - CALIBRATE
    }).join('-') : false;
}

module.exports.encode = encode;
module.exports.decode = decode;
module.exports.encodeAtChar = encodeAtChar;
