// // 1.

module.exports.largerThan = function (sumOfArray, maxNumber) {
    if(sumOfArray > maxNumber){
        return true;
    } else {
        return false;
    }

}

// 2.
module.exports.totalAnimals = function(chickens, cows, pigs) {
    return (chickens *= 2) + (cows *= 4) + (pigs *= 4);
    
}

// 3.
const words = ['hello', 'world'];

module.exports.wordLengths = function(words) {
    return words.map(word => word.length);
}
//     for(const item of words) {
//         let letters = item.split(',');
//         console.log(letters);
        
//         // for(let i = 0; i < letters.length; i++) {
//         //     return letters[i].length();
//         // }
        
//     }
    
// }

// 4.

module.exports.convertToRoman = function (num) {
    let result = '';
    let rom = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I'];
    let ara = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];

    for (let x = 0; x < rom.length; x++) {
        while (num >= ara[x]) {
          result += rom[x];
          num -= ara[x];
        }
      }
      return result;
    }
