// const people = [
//     {name: 'John', age: 21, budget: 29000},
//     {name: 'Steve', age: 32, budget: 32000},
//     {name: 'Martin', age: 16, budget: 1600}
// ]

module.exports.getBudgets = function(people) {
    return people.reduce(( prev, curr) => prev + curr.budget, 0);
}
// // console.log(getBudgets(people));


module.exports.vreplace = function (str, vowel) {
    let vowels = /[aeiou]/gi;
    return str.replace(vowels, vowel);
    
}

// // console.log(vreplace('stuffed jalapeno poppers', 'e'));


// module.exports.isIsogram = function(str) {

//     return str.split('').every((char, index) => str.indexOf(char) == index);
//     };

// // console.log(isIsogram('algorism'));



module.exports.isIsogram = function(str) {
    newStr = str.toLowerCase();
    return newStr.split('').every((char, index) => newStr.indexOf(char) == index);
    };

// console.log(isIsogram('algorism'));

