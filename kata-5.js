function square(numbers) {
    return numbers.map((num) => (num * num));
}

module.exports.square = square;


function charFreq(str) {
    const result= {};

    str.split('').forEach(char => {
        if(result[char]) result[char] += 1;
        else result[char] = 1;
    })
    return result
} 

module.exports.charFreq = charFreq;

function minMaxWords(words) {
    let short = words[0];
    let long = words[1];

    words.forEach(word => {
        if(word.length < short.length) {
            short = word;
        }
        if(word.length > long.length) {
            long = word;
        }
    })
    return [short, long];
}

module.exports.minMaxWords = minMaxWords;

function filterLongWords(maxLen, words) {
    return words.filter(word => word.length <= maxLen);
    }

module.exports.filterLongWords = filterLongWords;