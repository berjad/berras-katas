// 1. Write a function that calculates and adds 14% VAT to any given price. 
// If the price is less than 20, no VAT should he added, if the price is less than 0 
// it should return an error message

function calcVat(price, vatPerc = 14){
    if(price < 0){
        throw new error('bad');
    }
    if(price < 20){
        return price;
    }
    let vatToAdd = (price / 100) * vatPerc;
    return price + vatToAdd;
}
console.log(calcVat(50, 15))

// / 2. Write a function with 2 arguments, 
// argument 1 is an array of product Objects each with a price and name, 
//argument 2 is the name of a product. use a JavaScript native function to 
// find the price for the given name, from argument 1 list, the function 
//should be case insensitive
const products = [
    { price: 100, name: 'Coffee' },
    { price: 50, name: 'Cheese sandwich' },
    { price: 30, name: 'butter' }
];

function productPrice(products, productName) {
    const product = products.find(product => product.name.toLowerCase() == productName.toLowerCase());
    return product ? product.price : 'Product not found';
}
console.log(productPrice(products, 'butter'));
